#include "CPunkt.h"

class CRechteck:public CPunkt
{
public:
	CRechteck();
	CRechteck(CPunkt &a, CPunkt &b);	//Konstruktor mit Referenzparameter-�bergabe

	float getflaeche();
	void getpoints(CPunkt &a, CPunkt &b);

private:
	CPunkt mc;
	CPunkt md;
};