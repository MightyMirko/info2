#include "CPunkt.h"
#include "CRechteck.h"
using namespace std;




//TESTFALL 1: 2 Rechtecke schneiden sich nicht

void main()
{
	void check(CRechteck q1, CRechteck q2);

	float XS, YS, XS2, YS2;// Standardrechteck NA
	CPunkt pointS = CPunkt(1, -1);
	CPunkt pointS_2 = CPunkt(6, 5);
	pointS.get(XS, YS);
	pointS_2.get(XS2, YS2);
	cout << "Das Standardrechteck hat folgende Punkte: (" << XS << "," << YS << ")" << "(" <<XS2 <<","<<YS2 << ")" << endl;

	for (int i = 1,i=5, i++)
	{
		cout << "Das " << i << ". Rechteck wird konfiguriert" << endl;

		for (int n = 1, n=2, n++)
		{
			float x
			float y
			cout << "Geben sie X und Y Werte des " << n << ". Punkt des " << i << ". Rechtecks ein" << endl;

				cin >> x;
				cin >> y;
				CPunkt point*i = CPunkt(x, y);

		};

	};
	






	float x, y;
	CPunkt point = CPunkt(4, 5);
	point.get(x,y);
	cout << "Punkt 0: " << endl;
	cout << "x: " << x << endl << "y: " << y << endl;
	
	float x1, y1;
	CPunkt point1 = CPunkt(8, 10);
	point1.get(x1, y1);
	cout << "Punkt 1: " << endl;
	cout << "x: " << x1 << endl << "y: " << y1 << endl;

	CRechteck quarter = CRechteck(point, point1);
	cout << "Flaeche des 1. Rechteckes: " << quarter.getflaeche() << endl;
	

	float x2, y2;
	CPunkt point2 = CPunkt(7, 2);
	point2.get(x2, y2);
	cout << "Punkt 2: " << endl;
	cout << "x: " << x2 << endl << "y: " << y2 << endl;

	float x3, y3;
	CPunkt point3 = CPunkt(12, 20);
	point3.get(x3, y3);
	cout << "Punkt 3: " << endl;
	cout << "x: " << x3 << endl << "y: " << y3 << endl;

	CRechteck quarter1 = CRechteck(point2, point3);
	cout << "Flaeche des 2. Rechteckes: " << quarter1.getflaeche() << endl;

	check(quarter, quarter1);






	system("PAUSE");
}

void check(CRechteck q1, CRechteck q2)
{
	CPunkt pa1 = CPunkt();		//1. Rechteck, links unten	(A1)
	CPunkt pb1 = CPunkt();		//1. Rechteck, recht oben	(B1)
	CPunkt pa2 = CPunkt();		//2. Rechteck, links unten	(A2)
	CPunkt pb2 = CPunkt();		//2. Rechteck, recht oben	(B2)
	q1.getpoints(pa1, pb1);		
	q2.getpoints(pa2, pb2);

	float xa1, ya1, xb1, yb1, xa2, ya2, xb2, yb2;
	pa1.get(xa1, ya1);
	pb1.get(xb1, yb1);
	pa2.get(xa2, ya2);
	pb2.get(xb2, yb2);


	if (xb1<xa2 || xa1>xb2)
	{
		cout << "Test erfolgreich! Die Rechtecke ueberschneiden sich nicht." << endl;
		//cout << "Die Rechtecke sind seitlich versetzt" << endl;
	}
	else 
		if (ya1 > yb2 || ya2 > yb1)
		{
		cout << "Test erfolgreich! Die Rechtecke ueberschneiden sich nicht." << endl;
		//cout << "Die Rechtecke liegen uebereinander" << endl;
		}
		else
		{
			cout << "Test fehlgeschlagen:" << endl;
			cout << "Die Rechtecke ueberschneiden sich" << endl;
		}
}