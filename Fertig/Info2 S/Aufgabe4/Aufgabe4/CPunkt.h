#pragma once
#include <iostream>
#include <cstdlib>
using namespace std;

class CPunkt
{
public:
	CPunkt();
	CPunkt(float x, float y);			//Konstruktor
	void set(float x, float y);			//set X, Y
	void get(float &x, float &y);						//get X, Y
	//void sety(float y);	//set Y
	//float gety();			//set Y

private:
	float mx;
	float my;
};

