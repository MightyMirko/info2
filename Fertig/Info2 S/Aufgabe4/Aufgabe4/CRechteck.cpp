#include "CPunkt.h"
#include "CRechteck.h"

CRechteck::CRechteck(CPunkt &a, CPunkt &b)
{
	float ax, ay, bx, by;
	a.get(ax, ay);
	b.get(bx, by);

	if (ax < bx&&ay < by)		//Vergleich der Koordinaten, A muss "links unten", B "rechts oben" sein!!! (Aufgabenstellung)
	{
		mc.set(ax, by);
		md.set(bx, ay);
	}
	else
		cout << "Punkt A liegt nicht links unten" << endl;
}

void CRechteck::getpoints(CPunkt &a, CPunkt &b)
{
	float x1, y1, x2, y2;
	mc.get(x1, y1);
	md.get(x2, y2);
	a.set(x1, y2);		//R�ckgabe: Punkt A links unten
	b.set(x2, y1);		//R�ckgabe: Punkt B rechts oben
}



float CRechteck::getflaeche()
{
	float x1, y1,x2,y2;		
	mc.get(x1, y1);					//x1,y1 sind Koordinaten von mc
	md.get(x2, y2);					//x2,y2 sind Koordinaten von md
	return ((x2 - x1)*(y1 - y2));	//Return x*y, x1 ist < x2, y1 ist > y2 
									//Differenz korrekt bilden
}