#include "CPunkt.h"

CPunkt::CPunkt()
{
	mx = 0;
	my = 0;
}

CPunkt::CPunkt(float x, float y)
{
	mx = x;
	my = y;
}

void CPunkt::set(float x, float y)
{
	mx = x;
	my = y;
}

void CPunkt::get(float &x, float &y)
{
	x = mx;
	y = my;
}
