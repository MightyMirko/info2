#ifndef PIGGYBANK_H_
#define PIGGYBANK_H_

#include <iostream>
#include <string>
using namespace std;


class piggybank
{
	
public:
	piggybank(int n);
	int add1Cents(int n);
	int add10Cents(int n);
	int add50Cents(int n);
	int add1Euros(int n);
	bool isEmpty();
	bool isFull();
	bool isBroken();
	int breakInto();
	
private:
	int m1C;
	int m10C;
	int m50C;
	int m1E;
	int mMax;
	int mCount;
	bool mIsBroken;
};

#endif /* BINARYTREE_H_ */