#include "piggybank.h"

piggybank::piggybank(int max)
{
	mMax = max;
}

int piggybank::add1Cents(int n1C)
{
	if ((mCount + n1C) < mMax)
	{
		m1C=+ n1C;
		mCount = +n1C;
		return 0;
	}
		else
		{
			while (mCount != mMax)
			{
				m1C++;
				mCount++;
				n1C--;
			}
			return n1C;
		}
}

int piggybank::add10Cents(int n10C)
{
	if ((mCount + n10C) < mMax)
	{
		m10C = +n10C;
		mCount = +n10C;
		return 0;
	}
	else
	{
		while (mCount != mMax)
		{
			m10C++;
			mCount++;
			n10C--;
		}
		return n10C;
	}
}

int piggybank::add50Cents(int n50C)
{
	if ((mCount + n50C) < mMax)
	{
		m50C = +n50C;
		mCount = +n50C;
		return 0;
	}
	else
	{
		while (mCount != mMax)
		{
			m50C++;
			mCount++;
			n50C--;
		}
		return n50C;
	}
}

int piggybank::add1Euros(int n1E)
{
	if ((mCount + n1E) < mMax)
	{
		m1E = +n1E;
		mCount = +n1E;
		return 0;
	}
	else
	{
		while (mCount != mMax)
		{
			m1E++;
			mCount++;
			n1E--;
		}
		return n1E;
	}
}

bool piggybank::isEmpty()
{
	if (mCount == 0)
		return 1;
	else
		return 0;
}

bool piggybank::isFull()
{
	if (mCount == mMax)
		return 1;
	else
		return 0;
}

int piggybank::breakInto()
{
	return m1C + 10 * m10C + 50 * m50C + 100 * m1E;
	m1C = m10C = m50C = m1E = 0;
	mIsBroken = 1;
}


