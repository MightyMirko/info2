//Stack Header
//Labor 2
//02.11.2015


#ifndef STACK_H_
#define STACK_H_

#include <iostream>
#include <string>
using namespace std;


class CMessage
{
public:
	CMessage();
	CMessage(int Msg);							//ctor
	~CMessage();								//dtor, um Speicherplatz wieder zu erhalten
	int mID;
	int mMsg;
};



class CStack
{
public:
	CStack(int stacksize);						// ctor
	~CStack();									// dtor
	bool add(const CMessage& msg);				// add Message into the stack || ALIAS &msg (ADRESSE, in der MSG steht)
	bool get(CMessage& msg);					// get a Message from the stack
	int getNumOfMessages(void);					// get the number of Messages
	int getNumOfByteNeeded(void);				// get total number of data bytes occupied
	// by the stack
private:
	int mSize;									// size of stack
	int mCurrentSize;							// number of messages currently on the stack
	CMessage * mStackPtr;						// Stack Pointer
};


#endif											/* BINARYTREE_H_ */
