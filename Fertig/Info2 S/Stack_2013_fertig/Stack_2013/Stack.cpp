#include "Stack.h"
int x;
CStack::CStack(int size)
{
	mStackPtr = new CMessage[size];
	mSize = size;
	mCurrentSize = 0;
	//*mStackPtr[size] = {};					// +mCurrentSize;			//mStackPtr wandert eine Stelle weiter
}

CStack::~CStack()
{
	cout << "dtor Stack" << endl;
}

bool CStack::add(const CMessage& msg)
{
	if (mCurrentSize < mSize)
	{
		*mStackPtr = msg;								//Inhalt von mStackPtr ist aktuelle Message
		mStackPtr++;
		mCurrentSize++;									//Stack-Speicher um eine Zelle mehr bef�llt
		return 1;
	}

	else
		return 0;
}

bool CStack::get(CMessage& msg)
{
	if (mCurrentSize == 0)
		return 0;
	else
		{
		mStackPtr--;
		mCurrentSize--;
		msg = *mStackPtr;		//Inhalt des Stack-Pointers (Adresse von der aktuellen Message, da ALIAS) 
								//wird in msg geschrieben (ebenfalls ALIAS, hier wird nur mit Adressen gearbeitet)

		return 1;
		}
}

int CStack::getNumOfMessages()
{
	return mCurrentSize;
}

int CStack::getNumOfByteNeeded()
{
	return mCurrentSize * 8;
}

CMessage::CMessage(int Msg)
{
	mMsg = Msg;
	mID = x;
	x++;
}

CMessage::CMessage()
{
	mMsg = 0;
	mID = 0;
}

CMessage::~CMessage()
{
	cout << "Nachricht wurde geloescht" << endl;
}
