using namespace std;
#include <iostream>;

class ckonto
{
public:

	ckonto();		//constructor
	~ckonto();		//destructor
	ckonto(int);
	//ckonto1();
	//~ckonto1();
	int getkontonr();
	int getkontostand();
	//int getname();
	bool einzahlen(int wert);
	int setkontonr();
	int setkontostand();
	//int setname();

private:
	int mkontonr;
	int mkontostand;
	int mdispolimit;
};