#include "CContainer.h"

CContainer::CContainer()
{
	mOeldruck = 0;
	mWassertemp = 0;
	mZuendung = 0;
}

CContainer::CContainer(int oeldruck, float wassertemp, bool zuendung)
{
	mOeldruck = oeldruck;
	mWassertemp = wassertemp;
	mZuendung = zuendung;
}

int CContainer::setoeldruck(int &oeldruck)
{
	mOeldruck = oeldruck;
}

int CContainer::getoeldruck()
{
	cout << "Oeldruck: " << mOeldruck;
}

float CContainer::setwassertemp(float &wassertemp)
{
	mWassertemp = wassertemp;
}

float CContainer::getwassertemp()
{
	cout << "Wassertemperatur: " << mWassertemp;
}

bool CContainer::setzuendung(bool &zuendung)
{
	mZuendung = zuendung;
}
