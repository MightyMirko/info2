#ifndef CCONTAINER_H_
#define CCONTAINER_H_

#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

class CContainer
{
public:
	CContainer();
	CContainer(int oeldruck, float wassertemp, bool zuendung);
	int setoeldruck(int &oeldruck);				//Call-by-Reference: Variablen werden direkt in Funktion ge�ndert/beschrieben
	int getoeldruck();							//Weniger Speicherplatz ben�tigt, aber Variablen k�nnen direkt �berschrieben werden!!!
	float setwassertemp(float &wassertemp);		//ggf. const int &
	float getwassertemp();
	bool setzuendung(bool &zuendung);

private:
	int mOeldruck;
	float mWassertemp;
	bool mZuendung;

};


#endif