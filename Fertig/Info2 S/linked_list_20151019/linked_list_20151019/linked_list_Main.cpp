#include "linked_list_Header.h"
#include <algorithm>

using namespace std;

int main()
{
	//Erzeugen eines Pointers head, der auf den reservierten Speicher des neuen Elements zeigt
	//Dieses Element ist das "head-Element" ohne Namen
	CElement *head = new CElement;
	string name;

	while (1) 
	{
		cout << "Name:";
		cin >> name;

		//Funktion um Kleinbuchstaben zu "erzeugen"
		transform(name.begin(), name.end(), name.begin(), ::tolower);

		//Abbruchbedingung
		//Wenn "xxx" als Name eingegeben wird -> Ausstieg aus der while-Schleife
		if (name == "xxx")
			break;

		//Aufrufen der Methode Einfuegen des "head-Elements"
		head->Einfuegen(name);
	}

	//Ausgabe der zuvor eingegebenen Namen mithilfe der Funktion Traversieren
	head->Traversieren();

	system("PAUSE");
	return 0;
}