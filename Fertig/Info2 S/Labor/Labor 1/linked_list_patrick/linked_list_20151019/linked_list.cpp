#include "linked_list_Header.h"

CElement::CElement()
{
	mNextPtr = 0;
};

CElement::CElement(string Name)
{
	mName = Name;
};

CElement::~CElement()
{
	cout << "dtor" << endl;
};

bool CElement::Einfuegen(string name)
{
	//Erzeugen eines Pointers newElement, der auf den reservierten Speicher des neuen Elements zeigt
	//Diesem neuen Element wird der zuvor vom Benutzer eingegebene Name �bergeben
	CElement *newElement = new CElement(name);

	//Erzeugen eines Pointers next, der auf das aktuelle Element zeigt
	//im ersten Durchlauf ist dies das "head-Element"
	CElement *next = this;

	while (next->mNextPtr) 
	{
		
		//if-Schleife um den Namen des neuen Elements mit den vorhandenen Namen zu vergleichen
		//compare-Funktion, wenn der neue Name kleiner ist als einer aus der Liste, ist die if-Bedingung erf�llt
		if (name.compare(next->mNextPtr->mName) < 0) 
		{
			//Das neue Element wird vor dem aktuell verglichenen eingereiht
			newElement->mNextPtr = next->mNextPtr;
			break;
		}

		//weiterr�cken des next-pointers auf n�chstes Element
		next = next->mNextPtr;
	}

	//Hier wird dem Pointer mNextPtr des Vorg�nger-Elements das neue Element "zugewiesen"
	//Damit wird ein neues Element "eingehangen"
	next->mNextPtr = newElement;

	return true;
};

bool CElement::Traversieren()
{
	CElement *next = mNextPtr;

	while (next) {
		cout << next->mName << endl;
		next = next->mNextPtr;
	}

	return true;
};