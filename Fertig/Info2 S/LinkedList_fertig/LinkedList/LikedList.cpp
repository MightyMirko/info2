#include "LikedList.h"

CElement::CElement()
{
	//Erzeugen des Root-Elements mit der Adresse Root und mNextPtr = NULL
	cout << "ctor root-Element" << endl;
	mName = "";
	mNextPtr = NULL;
}

CElement::CElement(string NAME)
{
	mName = NAME;
}

CElement::~CElement()
{
	;
}

bool CElement::Einfuegen(string NAME)
{
	CElement *neu = new CElement(NAME);			//erzeugt temp. Element, das das neue Element darstellt
	//CElement *tmp = new CElement();			//erzeuge temporäres root-Element
	if (NAME != "xxx")
	{
		if (mNextPtr == 0)							//Fall 1, wenn Einfuegen() bei einem Element durchgeführt wird, das den NULL-Pointer als mNextPtr besitzt
		{
			mNextPtr = neu;
			neu->mNextPtr = NULL;
		}
		else
			if (mNextPtr->mName < NAME)				//Wenn nächstes Element kleiner als neu, gehe weiter
			{
			mNextPtr->Einfuegen(NAME);			//Rufe Einfuegen-Fkt. mit nächstem Element auf
			}
			else
			{									//Wenn mNextPtr nicht NULL ist und der Name des nächsten Elements NICHT kleiner ist als der einzufügende
												//also die richtige Stelle gefunden wurde
				neu->mNextPtr = mNextPtr;		//mNextPtr von neu = mNextPtr von aktuellem Element
				mNextPtr = neu;					//mNextPtr von aktuellem Element zeigt auf neu
				//i++;
			}
	}
	else
		return 1;

	return 1;
}

bool CElement::Traversieren()
{
		cout << mNextPtr->mName << endl;
		if (mNextPtr->mNextPtr == 0)
		{
			return 1;
		}
			else
			{
				mNextPtr->Traversieren(); 
			}
	return 1;
}

//bool CElement::setptr(CElement Name)
//{
//	*mNextPtr = Name;
//}

//bool CElement::getptr(CElement Name)
//{
//	*mNextPtr = Name;
//	return 1;
//}