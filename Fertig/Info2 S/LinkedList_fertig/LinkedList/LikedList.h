// demo fur Studies, Aufg.1 inf2, HSKA, 2015, JW
/*
* LinkedList.h
*
*  Created on: Oct 8, 2015
*      Author: Wietzke
*/

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_

#include <iostream>
#include <string>
using namespace std;


class CElement
{
//int i=0;
//int j = 0;
public:
	CElement();					// ctor ohne Param fuer Root Knoten
	CElement(string Name);		// ctor mit Werteingabe
	~CElement();				// dtor

	/*bool setptr(CElement Name);*/
	bool getptr(CElement Name);
	bool Einfuegen(string Name);		// Element einfuegen
	bool Traversieren();				// Alle Elemente ausgeben, es ist keine Rekursion aber sieht so aus
private:
	string mName;
	CElement * mNextPtr;
	//CElement *tmpptr;
};



#endif /* BINARYTREE_H_ */